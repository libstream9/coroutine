#include <stream9/coroutine/thread_pool.hpp>

#include <iostream> //TODO

namespace stream9::coroutine {

thread_pool::
thread_pool()
    : thread_pool { std::thread::hardware_concurrency() }
{}

thread_pool::
thread_pool(size_type const count)
{
    for (auto i = 0; i < count; ++i) {
        m_threads.emplace_back([&](std::stop_token t) { run(t); });
    }
}

thread_pool::
~thread_pool() noexcept
{
    for (auto& thread: m_threads) {
        thread.request_stop();
    }

    for (auto& thread: m_threads) {
        m_queue_event.notify_all();
        thread.join();
    }
}

thread_pool::size_type thread_pool::
thread_count() const noexcept
{
    return m_threads.size();
}

thread_pool::size_type thread_pool::
work_count()
{
    std::lock_guard lk { m_mutex };

    return m_work_queue.size();
}

void thread_pool::
execute(std::function<void()> work)
{
    std::lock_guard lk { m_mutex };

    m_work_queue.push_back(std::move(work));

    m_queue_event.notify_one();
}

void thread_pool::
wait()
{
    std::unique_lock lk { m_mutex };
    if (!m_work_queue.empty()) {
        m_empty_event.wait(lk, [&] { return m_work_queue.empty(); });
    }
}

void thread_pool::
run(std::stop_token token)
{
    std::unique_lock lk { m_mutex };

    while (!token.stop_requested()) {
        if (m_work_queue.empty()) {
            m_empty_event.notify_all();

            m_queue_event.wait(lk, [&] {
                return !m_work_queue.empty() || token.stop_requested();
            });

            if (token.stop_requested()) break;
        }

        auto work = std::move(m_work_queue.front());
        m_work_queue.pop_front();

        lk.unlock();

        work();

        lk.lock();
    }
}

} // namespace stream9::coroutine
