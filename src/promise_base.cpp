#include <stream9/coroutine/promise_base.hpp>

#include <stream9/coroutine/coroutine_ptr.hpp>
#include <stream9/coroutine/scheduler.hpp>

namespace stream9::coroutine {

class inline_scheduler_t : public scheduler
{
public:
    void schedule(coroutine_ptr<void> c) override
    {
        c.resume();
    }

    void schedule(lx::fd_ref/*src*/, uint32_t/*events*/, coroutine_ptr<> c) override
    {
        c.resume();
    }

    void deschedule(coroutine_ptr<>) override {}
};

static auto&
inline_scheduler()
{
    static inline_scheduler_t instance {};
    return instance;
}

promise_base::
promise_base()
    : m_scheduler { &inline_scheduler() }
{}

} // namespace stream9::coroutine
