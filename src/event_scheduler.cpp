#include <stream9/coroutine/event_scheduler.hpp>

namespace stream9::coroutine {

static uint32_t
accumulate_waiting_events(auto& waiting, lx::fd_ref fd)
{
    uint32_t events = 0;

    for (auto&& w: waiting) {
        if (w.fd == fd) {
            events |= w.events;
        }
    }

    return events;
}

template<typename T>
struct impl
{
    T& m_p;

    impl(T& p) : m_p { p } {}

    void
    handle_event(lx::fd_ref fd, uint32_t events)
    {
        for (auto it = m_p.m_waiting.begin(); it != m_p.m_waiting.end();) {
            if (it->fd == fd && it->events & events) {
                m_p.schedule(it->co);

                it = m_p.m_waiting.erase(it);
            }
            else {
                ++it;
            }
        }

        auto e = accumulate_waiting_events(m_p.m_waiting, fd);
        if (e == 0) { // no one is waiting fd anymore
            m_p.m_epoll.remove(fd);
        }
        else {
            m_p.m_epoll.modify(fd, e);
        }
    }

    void
    poll_event() noexcept // run on thread
    {
        try {
            while (true) {
                try {
                    auto events = m_p.m_epoll.wait();

                    std::lock_guard lk { m_p.m_waiting_mutex };
                    for (auto&& ev: events) {
                        auto fd = ev.data.fd;
                        if (fd == m_p.m_stop_event) {
                            return;
                        }
                        else {
                            handle_event(fd, ev.events);
                        }
                    }
                }
                catch (...) {
                    std::lock_guard lk { m_p.m_poller_error_mutex };
                    m_p.m_poller_error.insert(std::current_exception());
                }
            }
        }
        catch (...) {
            std::lock_guard lk { m_p.m_poller_error_mutex };
            m_p.m_poller_error.insert(std::current_exception());
        }
    }
};

/*
 * class event_scheduler
 */
event_scheduler::
event_scheduler()
    try : m_poller { [&] { impl(*this).poll_event(); }}
{
    m_epoll.add(m_stop_event, EPOLLIN);
}
catch (...) {
    rethrow_error();
}

event_scheduler::
~event_scheduler() noexcept
{
    try {
        if (m_poller.joinable()) {
            m_stop_event.write(1);
            m_poller.join();
        }
    }
    catch (...) {}
}

array<std::exception_ptr> event_scheduler::
errors()
{
    try {
        std::lock_guard lk { m_poller_error_mutex };
        return m_poller_error;
    }
    catch (...) {
        rethrow_error();
    }
}

void event_scheduler::
schedule(lx::fd_ref s, uint32_t events, coroutine_ptr<> co)
{
    try {
        std::lock_guard lk { m_waiting_mutex };

        auto ev = events | EPOLLERR | EPOLLHUP;

        auto e = accumulate_waiting_events(m_waiting, s);
        if (e == 0) {
            m_epoll.add(s, ev);
        }
        else {
            m_epoll.modify(s, e | ev);
        }

        st9::emplace_back(m_waiting, s, ev, co);

        this->add_suspended(co);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::coroutine
