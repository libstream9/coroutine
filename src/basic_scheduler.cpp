#include <stream9/coroutine/basic_scheduler.hpp>

namespace stream9::coroutine {

basic_scheduler::
basic_scheduler(class executor& e/*= inline_executor()*/)
    : m_executor { &e }
{}

void basic_scheduler::
schedule(coroutine_ptr<> p)
{
    std::lock_guard lk { m_queue_mutex };

    std::erase_if(m_suspended, [&](auto&& v) { return v == p; });

    p.set_scheduler(*this);

    m_ready.push_back(p);
    m_ready_event.notify_one();
}

void basic_scheduler::
schedule(lx::fd_ref/*src*/, uint32_t/*events*/, coroutine_ptr<> p)
{
    schedule(p);
}

void basic_scheduler::
deschedule(coroutine_ptr<> p)
{
    std::lock_guard lk { m_queue_mutex };

    std::erase_if(m_ready, [&](auto&& v) { return v == p; });
    std::erase_if(m_suspended, [&](auto&& v) { return v == p; });
}

void basic_scheduler::
add_suspended(coroutine_ptr<> p)
{
    std::lock_guard lk { m_queue_mutex };

    m_suspended.push_back(p);
}

class executor& basic_scheduler::
executor() const
{
    return *m_executor;
}

void basic_scheduler::
set_executor(class executor& e)
{
    m_executor = &e;
}

void basic_scheduler::
run()
{
    while (!m_stop_requested) {
        for (auto p = next_ready(); p != nullptr; p = next_ready()) {
            m_executor->execute([p, this] {
                p.resume();

                done_executing(p);
            });
        }

        {
            std::unique_lock queue_lock { m_queue_mutex };
            if (!m_ready.empty()) {
                continue;
            }
            else if (has_executing() || has_waiting()) {
                if (!m_stop_requested) {
                    m_ready_event.wait(queue_lock);
                }
            }
            else {
                break;
            }
        }
    }
}

void basic_scheduler::
shutdown() noexcept
{
    m_stop_requested = true;
    m_ready_event.notify_one();
}

coroutine_ptr<> basic_scheduler::
next_ready()
{
    std::lock_guard lk { m_queue_mutex };

    if (m_ready.empty()) return {};

    auto p = m_ready.front();
    m_ready.pop_front();

    m_executing.push_back(p);

    return p;
}

bool basic_scheduler::
has_executing() const
{
    return !m_executing.empty();
}

bool basic_scheduler::
has_waiting() const
{
    auto const it = std::find_if(
        m_suspended.begin(), m_suspended.end(),
        [](auto&& p) { return !p.done(); }
    );

    return it != m_suspended.end();
}

void basic_scheduler::
done_executing(coroutine_ptr<> const& p)
{
    std::lock_guard lk { m_queue_mutex };

    std::erase_if(m_executing, [&](auto&& v) { return v == p; });

    if (!p.done()) {
        m_suspended.push_back(p);
    }

    m_ready_event.notify_all();
}

} // namespace stream9::coroutine
