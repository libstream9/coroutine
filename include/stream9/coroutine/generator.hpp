#ifndef STREAM9_COROUTINE_GENERATOR_HPP
#define STREAM9_COROUTINE_GENERATOR_HPP

#include <coroutine>
#include <exception>
#include <utility>

#include <stream9/iterators.hpp>

namespace stream9::coroutine {

namespace generator_ { template<typename> class promise; }

template<typename T>
class generator
{
    static_assert(!std::same_as<T, void>);
    static_assert(!std::is_rvalue_reference_v<T>);
public:
    class iterator;

    using promise_type = generator_::promise<T>;
    using value_type = T;

private:
    using handle_t = std::coroutine_handle<promise_type>;

public:
    generator() = default; // partially-formed state

    generator(handle_t h) noexcept
        : m_handle { std::move(h) }
    {}

    ~generator() noexcept
    {
        if (m_handle) {
            m_handle.destroy();
        }
    }

    generator(generator const&) = delete;
    generator& operator=(generator const&) = delete;

    generator(generator&& other) noexcept
        : m_handle { std::move(other.m_handle) }
    {
        m_handle = nullptr;
    }

    generator& operator=(generator&& other) noexcept
    {
        swap(other);
        return *this;
    }

    // accessor
    iterator begin() noexcept { return m_handle; }
    iterator end() noexcept { return {}; }

    // query
    bool empty() const { return m_handle.done(); }

    // modifier
    void swap(generator& other) noexcept
    {
        using std::swap;
        swap(m_handle, other.m_handle);
    }

private:
    handle_t m_handle = nullptr;
};

namespace generator_ {

    template<typename T>
    class promise_base
    {
        using value_type = std::remove_reference_t<T>;
    public:
        value_type& value() const noexcept
        {
            return *m_value;
        }

        void set_value(value_type& v) noexcept
        {
            m_value = std::addressof(v);
        }

        void set_value(value_type&& v) noexcept
        {
            m_value = std::addressof(v);
        }

    private:
        value_type* m_value;
    };

    template<typename T>
    class promise : public promise_base<T>
    {
    public:
        std::suspend_never initial_suspend() const noexcept { return {}; }

        std::suspend_always final_suspend() const noexcept { return {}; }

        generator<T> get_return_object()
        {
            return std::coroutine_handle<promise>::from_promise(*this);
        }

        std::suspend_always yield_value(std::remove_reference_t<T>& v)
        {
            this->set_value(v);
            return {};
        }

        std::suspend_always yield_value(std::remove_reference_t<T>&& v)
        {
            this->set_value(std::move(v));
            return {};
        }

        void return_void() {}

        void unhandled_exception()
        {
            std::rethrow_exception(std::current_exception());
        }
    };

} // generator_

template<typename T>
class generator<T>::iterator : public stream9::iterators::iterator_facade<
                                        iterator,
                                        std::input_iterator_tag,
                                        std::conditional_t<
                                            std::is_reference_v<T>, T, T& >
                                      >
{
public:
    iterator() = default;
    iterator(generator<T>::handle_t& handle)
        : m_handle { &handle }
    {}

private:
    friend class stream9::iterators::iterator_core_access;

    decltype(auto) dereference() const
    {
        return m_handle->promise().value();
    }

    void increment()
    {
        m_handle->resume();
    }

    bool equal(iterator const& other) const
    {
        if (m_handle) [[likely]] {
            if (other.m_handle) {
                return m_handle->done() && other.m_handle->done();
            }
            else [[likely]] {
                return m_handle->done();
            }
        }
        else {
            if (other.m_handle) {
                return other.m_handle->done();
            }
            else {
                return true;
            }
        }
    }

private:
    generator<T>::handle_t* m_handle = nullptr;
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_GENERATOR_HPP
