#ifndef STREAM9_COROUTINE_WRITE_HPP
#define STREAM9_COROUTINE_WRITE_HPP

#include "task.hpp"
#include "writable.hpp"

#include <stream9/array_view.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/number.hpp>
#include <stream9/outcome.hpp>

#include <coroutine>

#include <unistd.h>

namespace stream9::coroutine {

inline task<outcome<natural<::ssize_t>, lx::errc>>
write(lx::fd_ref fd, array_view<char const> buf)
{
    while (true) {
        auto n = ::write(fd, buf.data(), buf.size());
        if (n == -1) {
            if (errno == EWOULDBLOCK) {
                co_await writable(fd);
            }
            else {
                co_return { st9::error_tag(), static_cast<lx::errc>(errno) };
            }
        }
        else {
            co_return n;
        }
    }
}

template<typename T>
inline task<outcome<natural<::ssize_t>, lx::errc>>
write(lx::fd_ref fd, T const& buf)
    requires (!std::convertible_to<T, array_view<char const>>)
{
    while (true) {
        auto n = ::write(fd, &buf, sizeof(buf));
        if (n == -1) {
            if (errno == EWOULDBLOCK) {
                co_await writable(fd);
            }
            else {
                co_return { st9::error_tag(), static_cast<lx::errc>(errno) };
            }
        }
        else {
            co_return n;
        }
    }
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_WRITE_HPP
