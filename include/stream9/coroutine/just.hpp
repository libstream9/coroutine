#ifndef STREAM9_COROUTINE_JUST_HPP
#define STREAM9_COROUTINE_JUST_HPP

#include "task.hpp"

namespace stream9::coroutine {

namespace just_ {

    struct api
    {
        template<typename T>
        task<T>
        operator()(T v) const
        {
            co_return v;
        }
    };

} // namespace just_

inline constexpr just_::api just;

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_JUST_HPP
