#ifndef STREAM9_COROUTINE_NAMESPACE_HPP
#define STREAM9_COROUTINE_NAMESPACE_HPP

namespace stream9 {

namespace coroutine {}

namespace co = coroutine;

} // namespace stream9

namespace st9 = stream9;

#endif // STREAM9_COROUTINE_NAMESPACE_HPP
