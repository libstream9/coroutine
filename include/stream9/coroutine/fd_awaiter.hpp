#ifndef STREAM9_COROUTINE_FD_AWAITER_HPP
#define STREAM9_COROUTINE_FD_AWAITER_HPP

#include <stream9/linux/fd.hpp>
#include <stream9/coroutine/event_scheduler.hpp>

#include <cstdint>

#include <sys/epoll.h>

namespace stream9::coroutine {

class fd_awaiter
{
public:
    fd_awaiter(lx::fd_ref fd, ::uint32_t events) noexcept
        : m_fd { fd }
        , m_events { events }
    {}

    bool await_ready() const noexcept
    {
        return false;
    }

    template<typename P>
    void await_suspend(std::coroutine_handle<P> h) const
    {
        try {
            auto& sched = h.promise().scheduler();

            sched.schedule(m_fd, m_events, coroutine_ptr(h));
        }
        catch (...) {
            rethrow_error();
        }
    }

    void await_resume() const noexcept {}

protected:
    lx::fd_ref m_fd;
    ::uint32_t m_events;
};

inline auto
readable(lx::fd_ref fd) noexcept
{
    return fd_awaiter(fd, EPOLLIN);
}

inline auto
writable(lx::fd_ref fd) noexcept
{
    return fd_awaiter(fd, EPOLLOUT);
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_FD_AWAITER_HPP
