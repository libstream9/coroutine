#ifndef STREAM9_COROUTINE_RESULT_HPP
#define STREAM9_COROUTINE_RESULT_HPP

#include <cassert>
#include <coroutine>
#include <exception>
#include <variant>

namespace stream9::coroutine {

template<typename T>
class result
{
public:
    // query
    bool empty() const
    {
        return std::holds_alternative<std::monostate>(m_result);
    }

    bool has_value() const
    {
        return std::holds_alternative<T>(m_result);
    }

    bool has_error() const
    {
        return std::holds_alternative<std::exception_ptr>(m_result);
    }

    T& get() &
    {
        assert(!empty());

        if (auto* const v = std::get_if<T>(&m_result)) {
            return *v;
        }
        else {
            std::rethrow_exception(std::get<std::exception_ptr>(m_result));
        }
    }

    T&& get() &&
    {
        return std::move(static_cast<result const*>(this)->get());
    }

    // modifier
    void set_value(T const& v)
    {
        m_result = v;
    }

    void set_value(T&& v)
    {
        m_result = std::move(v);
    }

    void set_error(std::exception_ptr const e)
    {
        m_result = e;
    }

private:
    std::variant<std::monostate, T, std::exception_ptr> m_result;
};

template<typename T>
class result<T&>
{
public:
    // query
    bool empty() const
    {
        return std::holds_alternative<std::monostate>(m_result);
    }

    bool has_value() const
    {
        return std::holds_alternative<T*>(m_result);
    }

    bool has_error() const
    {
        return std::holds_alternative<std::exception_ptr>(m_result);
    }

    T& get() const&
    {
        assert(!empty());

        if (auto* const v = std::get_if<T*>(&m_result)) {
            return **v;
        }
        else {
            std::rethrow_exception(std::get<std::exception_ptr>(m_result));
        }
    }

    T&& get() &&
    {
        return std::move(static_cast<result const*>(this)->get());
    }

    // modifier
    void set_value(T& v)
    {
        m_result = &v;
    }

    void set_error(std::exception_ptr const e)
    {
        m_result = e;
    }

private:
    std::variant<std::monostate, T*, std::exception_ptr> m_result;
};

template<typename T>
class result<T&&> : public result<T> {};

template<>
class result<void>
{
public:
    // query
    bool empty() const
    {
        return !has_error();
    }

    bool has_error() const
    {
        return m_error != nullptr;
    }

    void get() const
    {
        if (m_error) {
            std::rethrow_exception(m_error);
        }
    }

    // modifier
    void set_error(std::exception_ptr const e)
    {
        m_error = e;
    }

private:
    std::exception_ptr m_error = nullptr;
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_RESULT_HPP
