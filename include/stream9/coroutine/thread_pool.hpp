#ifndef STREAM9_COROUTINE_THREAD_POOL_HPP
#define STREAM9_COROUTINE_THREAD_POOL_HPP

#include "executor.hpp"
#include "namespace.hpp"

#include <condition_variable>
#include <cstdint>
#include <deque>
#include <functional>
#include <mutex>
#include <thread>
#include <vector>

#include <stream9/safe_integer.hpp>

namespace stream9::coroutine {

class thread_pool : public executor
{
public:
    using size_type = stream9::safe_integer<std::intmax_t, 0>;

public:
    thread_pool();
    thread_pool(size_type count);

    ~thread_pool() noexcept;

    // query
    size_type thread_count() const noexcept;
    size_type work_count();

    // command
    void execute(std::function<void()>) override;

    void wait();

private:
    void run(std::stop_token);

private:
    std::deque<std::function<void()>> m_work_queue;
    std::mutex m_mutex;
    std::condition_variable m_queue_event; //TODO semaphore
    std::condition_variable m_empty_event;
    std::vector<std::jthread> m_threads;
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_THREAD_POOL_HPP
