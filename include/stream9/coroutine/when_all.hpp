#ifndef STREAM9_COROUTINE_WHEN_ALL_HPP
#define STREAM9_COROUTINE_WHEN_ALL_HPP

#include "task.hpp"
#include "type_traits.hpp"

#include <tuple>
#include <utility>

namespace stream9::coroutine {

namespace when_all_ {

    template<typename T>
    task<awaitable_result_t<T>>
    when_all_impl(T&& a)
    {
        if constexpr (std::same_as<awaitable_result_t<T>, void>) {
            co_await a;
        }
        else {
            co_return co_await a;
        }
    }

    template<typename T1, typename T2>
    task<awaitable_result_t<T1, T2>>
    when_all_impl(T1&& a1, T2&& a2)
    {
        if constexpr (std::same_as<awaitable_result_t<T1, T2>, void>) {
            co_await a1;
            co_await a2;
        }
        else if constexpr (std::same_as<awaitable_result_t<T1>, void>) {
            co_await a1;
            co_return co_await a2;
        }
        else if constexpr (std::same_as<awaitable_result_t<T2>, void>) {
            co_await a2;
            co_return co_await a1;
        }
        else {
            co_return std::make_tuple(co_await a1, co_await a2);
        }
    }

    template<typename>
    struct is_tuple : std::false_type {};

    template<typename... Ts>
    struct is_tuple<std::tuple<Ts...>> : std::true_type {};

    template<typename T, typename... Rest>
    task<awaitable_result_t<T, Rest...>>
    when_all_impl(T&& a, Rest&&... rest)
    {
        if constexpr (std::same_as<awaitable_result_t<T>, void>) {
            co_await a;
            co_return co_await (when_all_impl)(std::forward<Rest>(rest)...);
        }
        else {
            using rest_result_t = awaitable_result_t<Rest...>;

            if constexpr (std::same_as<rest_result_t, void>) {
                co_return co_await a;
            }
            if constexpr (is_tuple<rest_result_t>::value) {
                co_return std::tuple_cat(
                    std::make_tuple(co_await a),
                    co_await (when_all_impl)(std::forward<Rest>(rest)...)
                );
            }
            else {
                co_return std::make_tuple(
                    co_await a,
                    co_await (when_all_impl)(std::forward<Rest>(rest)...)
                );
            }
        }
    }

    struct api
    {
        template<awaitable... Ts>
        decltype(auto)
        operator()(Ts&&... a) const
        {
            return (when_all_impl)(std::forward<Ts>(a)...);
        }
    };

} // namespace when_all_

inline constexpr when_all_::api when_all;

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_WHEN_ALL_HPP
