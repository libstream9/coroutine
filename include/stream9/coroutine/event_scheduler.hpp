#ifndef STREAM9_COROUTINE_EVENT_SCHEDULER_HPP
#define STREAM9_COROUTINE_EVENT_SCHEDULER_HPP

#include "namespace.hpp"
#include "basic_scheduler.hpp"
#include "coroutine_ptr.hpp"
#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/errors.hpp>
#include <stream9/linux/epoll.hpp>
#include <stream9/linux/eventfd.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/find.hpp>

#include <cstdint>
#include <mutex>
#include <thread>

namespace stream9::coroutine {

class event_scheduler : public basic_scheduler
{
public:
    // essentials
    event_scheduler();

    event_scheduler(event_scheduler const&) = delete;
    event_scheduler& operator=(event_scheduler const&) = delete;

    event_scheduler(event_scheduler&&) noexcept = default;
    event_scheduler& operator=(event_scheduler&&) noexcept = default;

    ~event_scheduler() noexcept;

    // query
    array<std::exception_ptr> errors();

    // modifier
    void schedule(lx::fd_ref src, uint32_t events, coroutine_ptr<>) override;

    using basic_scheduler::schedule;

private:
    template<typename> friend struct impl;

    struct waiting {
        lx::fd_ref fd;
        uint32_t events;
        coroutine_ptr<> co;
    };

    array<std::exception_ptr> m_poller_error;
    array<waiting> m_waiting;
    lx::epoll<> m_epoll;
    lx::eventfd m_stop_event;
    std::mutex m_poller_error_mutex;
    std::mutex m_waiting_mutex;
    std::jthread m_poller;
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_EVENT_SCHEDULER_HPP
