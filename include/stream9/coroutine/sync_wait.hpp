#ifndef STREAM9_COROUTINE_SYNC_WAIT_HPP
#define STREAM9_COROUTINE_SYNC_WAIT_HPP

#include "promise_result.hpp"
#include "task.hpp"
#include "type_traits.hpp"
#include "result.hpp"

#include <chrono>
#include <condition_variable>
#include <coroutine>
#include <thread>

#include <boost/thread/latch.hpp>

#include <iostream>

namespace stream9::coroutine {

namespace sync_wait_ {

    template<typename> class promise;

    template<typename T>
    struct wait_task {
        using promise_type = promise<T>;
    };

    template<typename T>
    class promise : public promise_result<T>
    {
    public:
        promise(result<T>& r, auto&, boost::latch&)
            : promise_result<T> { r }
        {}

        constexpr std::suspend_never initial_suspend() const noexcept { return {}; }
        constexpr std::suspend_never final_suspend() const noexcept { return {}; }

        constexpr wait_task<T> get_return_object() const noexcept { return {}; }
    };

    template<typename Rep, typename Period>
    inline constexpr auto
    to_boost_time_point(std::chrono::time_point<std::chrono::steady_clock,
                                std::chrono::duration<Rep, Period> > const& from)
    {
        using period_t = boost::ratio<Period::num, Period::den>;
        using duration_t = boost::chrono::duration<Rep, period_t>;
        using result_t = boost::chrono::time_point<
                    boost::chrono::steady_clock, duration_t >;

        return result_t {
            duration_t { from.time_since_epoch().count() }
        };
    }

    template<typename T>
    wait_task<T>
    start_waiting(result<T>&, auto& a, boost::latch& l)
    {
        if constexpr (std::same_as<T, void>) {
            co_await a;
            l.count_down();
        }
        else {
            decltype(auto) rv = co_await a;

            l.count_down();

            co_return rv;
        }
    }

    struct wait_api {
        template<awaitable T>
        decltype(auto)
        operator()(T&& a) const
        {
            boost::latch latch { 1 };

            result<awaitable_result_t<T>> r;

            start_waiting(r, a, latch);

            latch.wait();

            return r.get();
        }
    };

    struct wait_until_api {
        template<awaitable T, typename Clock, typename Duration>
        std::cv_status
        operator()(T&& a, std::chrono::time_point<Clock, Duration> const& tp) const
        {
            boost::latch latch { 1 };

            result<awaitable_result_t<T>> r;

            start_waiting(r, a, latch);

            auto const rv = latch.wait_until(to_boost_time_point(tp));

            return rv == boost::cv_status::timeout ?
                std::cv_status::timeout : std::cv_status::no_timeout;
        }
    };

    struct wait_for_api {
        template<awaitable T, typename Rep, typename Period>
        std::cv_status
        operator()(T&& a, std::chrono::duration<Rep, Period> const& d) const
        {
            wait_until_api wait_until;

            return wait_until(a, std::chrono::steady_clock::now() + d);
        }
    };

} // namespace sync_wait_

inline constexpr sync_wait_::wait_api sync_wait;
inline constexpr sync_wait_::wait_for_api sync_wait_for;
inline constexpr sync_wait_::wait_until_api sync_wait_until;

/*
 * class task
 */
template<typename T>
decltype(auto) task<T>::
wait()
{
    return sync_wait(*this);
}

template<typename T>
template<typename Rep, typename Period>
std::cv_status task<T>::
wait_for(std::chrono::duration<Rep, Period> const& d)
{
    return sync_wait_for(*this, d);
}

template<typename T>
template<typename Clock, typename Duration>
std::cv_status task<T>::
wait_until(std::chrono::time_point<Clock, Duration> const& tp)
{
    return sync_wait_until(*this, tp);
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_SYNC_WAIT_HPP
