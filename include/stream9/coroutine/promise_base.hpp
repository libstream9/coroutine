#ifndef STREAM9_COROUTINE_PROMISE_BASE_HPP
#define STREAM9_COROUTINE_PROMISE_BASE_HPP

#include "reference_counter.hpp"

#include <coroutine>

namespace stream9::coroutine {

class scheduler;

class promise_base
{
public:
    promise_base();

    // reference counter
    std::ptrdiff_t ref() noexcept
    {
        return m_ref_counter.ref();
    }

    std::ptrdiff_t unref() noexcept
    {
        return m_ref_counter.unref();
    }

    // continuation
    auto continuation() const noexcept { return m_continuation; }

    template<typename Promise>
    void set_continuation(std::coroutine_handle<Promise> h) noexcept
    {
        m_continuation = h;
    }

    // scheduler
    class scheduler& scheduler() const noexcept { return *m_scheduler; }

    void set_scheduler(class scheduler& e) noexcept
    {
        m_scheduler = &e;
    }

private:
    reference_counter m_ref_counter;
    std::coroutine_handle<> m_continuation = std::noop_coroutine();
    class scheduler* m_scheduler; // non-null
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_PROMISE_BASE_HPP
