#ifndef STREAM9_COROUTINE_TYPE_TRAITS_HPP
#define STREAM9_COROUTINE_TYPE_TRAITS_HPP

#include <concepts>
#include <coroutine>
#include <utility>

namespace stream9::coroutine {

    /*
     * coroutine
     */
    template<typename R, typename... Args>
    constexpr bool is_coroutine_v(R(Args...))
    {
        return requires {
            typename std::coroutine_traits<R, Args...>::promise_type;
        };
    }

    template<typename T, typename R, typename... Args>
    constexpr bool is_coroutine_v(R(T::*)(Args...))
    {
        return requires {
            typename std::coroutine_traits<R, T&, Args...>::promise_type;
        };
    }

    template<typename T, typename R, typename... Args>
    constexpr bool is_coroutine_v(R(T::*)(Args...) const)
    {
        return requires {
            typename std::coroutine_traits<R, T&, Args...>::promise_type;
        };
    }

    template<typename T, typename R, typename... Args>
    constexpr bool is_coroutine_v(R(T::*)(Args...) &)
    {
        return requires {
            typename std::coroutine_traits<R, T&, Args...>::promise_type;
        };
    }

    template<typename T, typename R, typename... Args>
    constexpr bool is_coroutine_v(R(T::*)(Args...) const&)
    {
        return requires {
            typename std::coroutine_traits<R, T&, Args...>::promise_type;
        };
    }

    template<typename T, typename R, typename... Args>
    constexpr bool is_coroutine_v(R(T::*)(Args...) &&)
    {
        return requires {
            typename std::coroutine_traits<R, T&, Args...>::promise_type;
        };
    }

    template<typename T, typename R, typename... Args>
    constexpr bool is_coroutine_v(R(T::*)(Args...) const&&)
    {
        return requires {
            typename std::coroutine_traits<R, T&, Args...>::promise_type;
        };
    }

    template<typename T>
        requires (is_coroutine_v(&T::operator()))
    constexpr bool is_coroutine_v(T)
    {
        return is_coroutine_v(&T::operator());
    }

    template<typename T>
    concept has_promise_type = requires {
        typename T::promise_type;
    };

    /*
     * awaiter
     */
    template<typename T>
    concept awaiter = requires (T t) {
        { t.await_ready() } -> std::convertible_to<bool>;
        //t.await_suspend(); //TODO
        t.await_resume();
    };

    /*
     * promise
     */
    template<typename Promise>
    concept has_await_transform = requires (Promise p) {
        { p.await_transform() } -> awaiter;
    };

    /*
     * awaitable
     */
    template<typename Awaitable>
    concept has_member_co_await = requires (Awaitable&& t) {
        std::forward<Awaitable>(t).operator co_await();
    };

    template<typename Awaitable>
    concept has_adl_co_await = requires (Awaitable&& t) {
        operator co_await(std::forward<Awaitable>(t));
    };

    template<typename T>
    concept awaitable = awaiter<T>
                     || (has_member_co_await<T>)
                     || (has_adl_co_await<T>);

    /*
     * awaiter_type
     */
    template<typename> struct awaiter_type;

    template<awaiter T>
    struct awaiter_type<T> { using type = T; };

    template<typename Awaitable>
        requires (!awaiter<Awaitable>)
              && (has_member_co_await<Awaitable>)
    struct awaiter_type<Awaitable>
    {
        using type =
            decltype(std::declval<Awaitable>().operator co_await());
    };

    template<typename Awaitable>
        requires (!awaiter<Awaitable>)
              && (!has_member_co_await<Awaitable>)
              && (has_adl_co_await<Awaitable>)
    struct awaiter_type<Awaitable>
    {
        using type = decltype(operator co_await(std::declval<Awaitable>()));
    };

    template<awaitable T>
    using awaiter_type_t = awaiter_type<T>::type;

    /*
     * awaitable_result
     */
    template<awaitable...> struct awaitable_result;

    template<awaitable... Ts>
    using awaitable_result_t = typename awaitable_result<Ts...>::type;

    namespace detail {

        template<typename, typename> struct tuple_con;

        template<typename T1, typename T2>
        struct tuple_con
        {
            using type = std::tuple<T1, T2>;
        };

        template<typename T, typename... Rest>
        struct tuple_con<T, std::tuple<Rest...>>
        {
            using type = std::tuple<T, Rest...>;
        };

        template<typename T1, typename T2>
        using tuple_con_t = tuple_con<T1, T2>::type;

        template<typename...> struct combine_result;

        template<typename... Ts>
        using combine_result_t = typename combine_result<Ts...>::type;

        template<typename T>
        struct combine_result<T, void>
        {
            using type = T;
        };

        template<typename T>
        struct combine_result<void, T>
        {
            using type = T;
        };

        template<>
        struct combine_result<void, void>
        {
            using type = void;
        };

        template<typename T1, typename T2>
        struct combine_result<T1, T2>
        {
            using type = std::tuple<T1, T2>;
        };

        template<typename... Rest>
        struct combine_result<void, Rest...>
        {
            using type = combine_result_t<Rest...>;
        };

        template<typename T, typename... Rest>
        struct combine_result<T, Rest...>
        {
            using type = tuple_con_t<T, combine_result_t<Rest...>>;
        };

    } // namespace detail

    template<awaitable T>
    struct awaitable_result<T>
    {
        using type = decltype(std::declval<awaiter_type_t<T>>().await_resume());
    };

    template<awaitable... T>
    struct awaitable_result
    {
        using type = detail::combine_result_t<awaitable_result_t<T>...>;
    };

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_TYPE_TRAITS_HPP
