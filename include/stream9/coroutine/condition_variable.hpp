#ifndef STREAM9_COROUTINE_CONDITION_VARIABLE_HPP
#define STREAM9_COROUTINE_CONDITION_VARIABLE_HPP

#include "coroutine_ptr.hpp"
#include "task.hpp"

#include <coroutine>
#include <deque>
#include <mutex>
#include <thread>

namespace stream9::coroutine {

class condition_variable
{
    class awaiter;

public:
    condition_variable() = default;

    ~condition_variable() = default;

    condition_variable(condition_variable const&) = delete;
    condition_variable& operator=(condition_variable const&) = delete;

    task<> wait(std::unique_lock<std::mutex>&);

    template<typename Predicate>
    task<> wait(std::unique_lock<std::mutex>&, Predicate);

    void notify_one()
    {
        std::unique_lock lk { m_mutex };
        if (m_coroutines.empty()) return;

        auto const coro = m_coroutines.front();
        coro.resume();

        m_coroutines.pop_front();
    }

    void notify_all()
    {
        std::unique_lock lk { m_mutex };

        while (!m_coroutines.empty()) {
            auto& coro = m_coroutines.front();
            coro.resume();

            m_coroutines.pop_front();
        }
    }

private:
    void enqueue(coroutine_ptr<> h)
    {
        std::unique_lock lk { m_mutex };
        m_coroutines.push_back(h);
    }

private:
    std::mutex m_mutex;
    std::deque<coroutine_ptr<>> m_coroutines;
};

class condition_variable::awaiter
{
public:
    awaiter(condition_variable& cv)
        : m_cv { cv }
    {}

    constexpr bool await_ready() const noexcept { return false; }

    template<typename Promise>
        requires std::derived_from<Promise, promise_base>
    void await_suspend(std::coroutine_handle<Promise> h)
    {
        auto ptr = h.promise().coroutine_ptr();

        m_cv.enqueue(std::move(ptr));
    }

    constexpr void await_resume() const noexcept {}

private:
    condition_variable& m_cv;
};

inline task<> condition_variable::
wait(std::unique_lock<std::mutex>& lk)
{
    lk.unlock();

    co_await awaiter { *this };

    lk.lock();
}

template<typename Predicate>
task<> condition_variable::
wait(std::unique_lock<std::mutex>& lk, Predicate pred)
{
    lk.unlock();

    while (!pred()) {
        co_await awaiter { *this };
    }

    lk.lock();
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_CONDITION_VARIABLE_HPP
