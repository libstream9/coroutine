#ifndef STREAM9_COROUTINE_SCHEDULER_HPP
#define STREAM9_COROUTINE_SCHEDULER_HPP

#include <stream9/linux/fd.hpp>

#include <sys/epoll.h>

namespace stream9::coroutine {

template<typename> class coroutine_ptr;

class scheduler
{
public:
    virtual ~scheduler() = default;

    virtual void schedule(coroutine_ptr<void>) = 0;
    virtual void schedule(lx::fd_ref src, uint32_t events, coroutine_ptr<void>) = 0;

    virtual void deschedule(coroutine_ptr<void>) = 0;
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_SCHEDULER_HPP
