#ifndef STREAM9_COROUTINE_TASK_HPP
#define STREAM9_COROUTINE_TASK_HPP

#include "coroutine_ptr.hpp"
#include "promise_base.hpp"
#include "promise_result.hpp"
#include "result.hpp"
#include "type_traits.hpp"

#include <cassert>
#include <chrono>
#include <condition_variable>
#include <coroutine>
#include <utility>

namespace stream9::coroutine {

namespace task_ { template<typename> class promise; }

template<typename T = void>
class [[nodiscard]] task
{
public:
    using promise_type = task_::promise<T>;

private:
    using handle_t = std::coroutine_handle<promise_type>;

public:
    task() = default; // partially-formed state

    template<awaitable A>
    task(A&&);

    task(task const&) = default;
    task& operator=(task const&) = default;

    task(task&&) = default;
    task& operator=(task&&) = default;

    // query
    bool done() const
    {
        return m_coroutine.done();
    }

    decltype(auto) get()
        requires (!std::same_as<T, void>)
    {
        wait();
        return result();
    }

    decltype(auto) result() &
        requires (!std::same_as<T, void>)
    {
        return m_coroutine.promise().result();
    }

    decltype(auto) result() &&
        requires (!std::same_as<T, void>)
    {
        return std::move(m_coroutine.promise()).result();
    }

    auto& coroutine_ptr() const { return m_coroutine; }
    auto& coroutine_ptr() { return m_coroutine; }

    // modifier
    void swap(task& other) noexcept
    {
        using std::swap;
        swap(m_coroutine, other.m_coroutine);
    }

    // command
    void resume()
    {
        assert(!done());
        m_coroutine.resume();
    }

    void operator()() { resume(); }

    decltype(auto) wait();

    template<typename Rep, typename Period>
    std::cv_status
    wait_for(std::chrono::duration<Rep, Period> const&);

    template<typename Clock, typename Duration>
    std::cv_status
    wait_until(std::chrono::time_point<Clock, Duration> const&);

    // conversion
    auto operator co_await() & noexcept;
    auto operator co_await() const& noexcept;
    auto operator co_await() && noexcept;
    auto operator co_await() const&& noexcept;

    operator class coroutine_ptr<> () const
    {
        return m_coroutine;
    }

protected:
    friend class task_::promise<T>;

    task(class coroutine_ptr<promise_type> ptr) noexcept
        : m_coroutine { ptr }
    {}

private:
    class coroutine_ptr<promise_type> m_coroutine;
};

namespace task_ {

    template<typename T>
    class promise : public promise_result<T>, public promise_base
    {
        class final_awaiter;

    public:
        promise()
            : promise_result<T> { m_result }
        {}

        std::suspend_always initial_suspend() const noexcept { return {}; }

        final_awaiter final_suspend() const noexcept;

        task<T> get_return_object()
        {
            return task<T> { coroutine_ptr() };
        }

        class coroutine_ptr<promise> coroutine_ptr()
        {
            return *this;
        }

        void schedule()
        {
            this->scheduler().schedule(coroutine_ptr());
        }

        void schedule(lx::fd_ref fd, uint32_t events)
        {
            this->scheduler().schedule(fd, events, coroutine_ptr());
        }

        void deschedule()
        {
            this->scheduler().deschedule(coroutine_ptr());
        }

    private:
        result<T> m_result;
    };

    template<typename T>
    class promise<T>::final_awaiter
    {
    public:
        constexpr bool await_ready() const noexcept { return false; }

        template<typename Promise>
        decltype(auto)
        await_suspend(std::coroutine_handle<Promise> h) const noexcept
        {
            auto& p = h.promise();

            auto c = p.continuation();

            p.deschedule();

            return c;
        }

        void await_resume() const noexcept {}
    };

    template<typename T>
    promise<T>::final_awaiter promise<T>::
    final_suspend() const noexcept
    {
        return {};
    }

    template<typename Promise>
    class task_awaiter_base
    {
    public:
        task_awaiter_base(std::coroutine_handle<Promise> task) noexcept
            : m_task { task }
        {}

        bool await_ready() const
        {
            return m_task.done();
        }

        template<typename T>
        std::coroutine_handle<>
        await_suspend(std::coroutine_handle<T> awaiting) noexcept
        {
            m_task.promise().set_continuation(awaiting);

            if constexpr (std::derived_from<T, promise_base>) {
                m_task.promise().set_scheduler(awaiting.promise().scheduler());
            }

            return m_task;
        }

    protected:
        std::coroutine_handle<Promise> m_task;
    };

    template<typename Promise>
    class task_awaiter : public task_awaiter_base<Promise>
    {
    public:
        task_awaiter(std::coroutine_handle<Promise> task) noexcept
            : task_awaiter_base<Promise> { task }
        {}

        decltype(auto)
        await_resume() const
        {
            return this->m_task.promise().result();
        }
    };

    template<typename Promise>
    class move_task_awaiter : public task_awaiter_base<Promise>
    {
    public:
        move_task_awaiter(std::coroutine_handle<Promise> task) noexcept
            : task_awaiter_base<Promise> { task }
        {}

        decltype(auto)
        await_resume() const
        {
            return std::move(this->m_task.promise()).result();
        }
    };

    template<typename T, typename Awaitable>
    task<T>
    make_task(Awaitable a)
    {
        co_return co_await std::move(a);
    }

} // namespace task_

template<typename T>
template<awaitable A>
task<T>::
task(A&& a)
    : task { task_::make_task<T>(std::forward<A>(a)) }
{}

template<awaitable A>
task(A&&) -> task<awaitable_result_t<A>>;

template<typename T>
auto task<T>::
operator co_await() & noexcept
{
    return task_::task_awaiter { m_coroutine.handle() };
}

template<typename T>
auto task<T>::
operator co_await() const& noexcept
{
    return task_::task_awaiter { m_coroutine.handle() };
}

template<typename T>
auto task<T>::
operator co_await() && noexcept
{
    return task_::move_task_awaiter { m_coroutine.handle() };
}

template<typename T>
auto task<T>::
operator co_await() const&& noexcept
{
    return task_::move_task_awaiter { m_coroutine.handle() };
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_TASK_HPP

#include "sync_wait.hpp"
