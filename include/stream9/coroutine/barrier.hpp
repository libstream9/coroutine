#ifndef STREAM9_COROUTINE_BARRIER_HPP
#define STREAM9_COROUTINE_BARRIER_HPP

#include "task.hpp"
#include "detail/waiting_queue.hpp"

namespace stream9::coroutine {

template<typename CompletionFunction = void*>
class barrier
{
public:
    barrier(std::ptrdiff_t expected, CompletionFunction = {});

    barrier(barrier const&) = delete;
    barrier& operator=(barrier const&) = delete;

    // command
    void arrive();
    void arrive_and_drop();

    task<> wait();
    task<> arrive_and_wait();

private:
    detail::waiting_queue m_queue;
    std::ptrdiff_t m_expected {};
    std::atomic_ptrdiff_t m_count {};
    CompletionFunction m_completion;
};

template<typename CompletionFunction>
inline barrier<CompletionFunction>::
barrier(std::ptrdiff_t const expected,
        CompletionFunction comp/*= {}*/)
    : m_expected { expected }
    , m_count { expected }
    , m_completion { std::move(comp) }
{}

template<typename CompletionFunction>
void barrier<CompletionFunction>::
arrive()
{
    if (m_count == 0) return;

    --m_count;
    if (m_count == 0) {
        m_count = m_expected;

        m_queue.release_all();

        if constexpr (std::invocable<CompletionFunction>) {
            m_completion();
        }
    }
}

template<typename CompletionFunction>
void barrier<CompletionFunction>::
arrive_and_drop()
{
    if (m_count == 0) return;

    --m_count;
    --m_expected;
    if (m_count == 0) {
        m_count = m_expected;

        m_queue.release_all();

        if constexpr (std::invocable<CompletionFunction>) {
            m_completion();
        }
    }
}

template<typename CompletionFunction>
task<> barrier<CompletionFunction>::
wait()
{
    if (m_count != 0) {
        co_await m_queue;
    }
}

template<typename CompletionFunction>
task<> barrier<CompletionFunction>::
arrive_and_wait()
{
    if (m_count != 0) {
        --m_count;
        if (m_count == 0) {

            m_queue.release_all();
        }
        else {
            co_await m_queue;
        }
    }
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_BARRIER_HPP
