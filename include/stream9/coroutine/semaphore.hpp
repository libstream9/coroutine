#ifndef STREAM9_COROUTINE_SEMAPHORE_HPP
#define STREAM9_COROUTINE_SEMAPHORE_HPP

#include "task.hpp"

#include "detail/waiting_queue.hpp"

#include <cstddef>
#include <atomic>

namespace stream9::coroutine {

template<std::ptrdiff_t least_max_value= PTRDIFF_MAX>
class counting_semaphore
{
public:
    constexpr explicit counting_semaphore(std::ptrdiff_t desired);

    ~counting_semaphore() = default;

    counting_semaphore(counting_semaphore const&) = delete;
    counting_semaphore& operator=(counting_semaphore const&) = delete;

    // query
    static std::ptrdiff_t max() noexcept;

    // command
    void release(std::ptrdiff_t update = 1);
    bool try_acquire() noexcept;
    task<> acquire();

private:
    std::atomic<ptrdiff_t> m_counter;
    detail::waiting_queue m_queue;
};

using binary_semaphore = counting_semaphore<1>;

template<std::ptrdiff_t least_max_value>
constexpr counting_semaphore<least_max_value>::
counting_semaphore(std::ptrdiff_t const desired)
    : m_counter { desired }
{
    assert(desired >= 0);
    assert(desired <= max());
}

template<std::ptrdiff_t least_max_value>
std::ptrdiff_t counting_semaphore<least_max_value>::
max() noexcept
{
    return least_max_value;
}

template<std::ptrdiff_t least_max_value>
void counting_semaphore<least_max_value>::
release(std::ptrdiff_t const update/*= 1*/)
{
    assert(update >= 0);

    auto counter = m_counter.load();
    assert(update <= max() - counter);

    while (!m_counter.compare_exchange_weak(counter, counter + update))
    {
        assert(update <= max() - counter);
    }

    if (counter == 0) {
        m_queue.release_all();
    }
}

template<std::ptrdiff_t least_max_value>
bool counting_semaphore<least_max_value>::
try_acquire() noexcept
{
    auto counter = m_counter.load();

    if (counter > 0) {
        while(!m_counter.compare_exchange_weak(counter, counter - 1)) {
            if (counter <= 0) return false;
        }
        return true;
    }
    else {
        return false;
    }
}

template<std::ptrdiff_t least_max_value>
task<> counting_semaphore<least_max_value>::
acquire()
{
    if (!try_acquire()) {
        co_await m_queue;
    }
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_SEMAPHORE_HPP
