#ifndef STREAM9_COROUTINE_DETAIL_WAITING_LIST_HPP
#define STREAM9_COROUTINE_DETAIL_WAITING_LIST_HPP

#include "../coroutine_ptr.hpp"
#include "../task.hpp"

#include <coroutine>
#include <deque>
#include <mutex>

namespace stream9::coroutine::detail {

class waiting_queue
{
    class awaiter;

public:
    waiting_queue() = default;

    ~waiting_queue() = default;

    waiting_queue(waiting_queue const&) = delete;
    waiting_queue& operator=(waiting_queue const&) = delete;

    task<> wait();
    void release_all();

    awaiter operator co_await() noexcept;

private:
    void enqueue(coroutine_ptr<>);

private:
    std::mutex m_mutex;
    std::deque<coroutine_ptr<>> m_queue;
};

class waiting_queue::awaiter
{
public:
    awaiter(waiting_queue& q)
        : m_queue { q }
    {}

    constexpr bool await_ready() const noexcept { return false; }

    template<typename Promise>
        requires std::derived_from<Promise, promise_base>
    void await_suspend(std::coroutine_handle<Promise> h)
    {
        auto& p = h.promise();
        auto ptr = p.coroutine_ptr();

        m_queue.enqueue(std::move(ptr));
    }

    constexpr void await_resume() const noexcept {}

private:
    waiting_queue& m_queue;
};

inline task<> waiting_queue::
wait()
{
    co_await awaiter { *this };
}

inline void waiting_queue::
release_all()
{
    decltype(m_queue) queue;
    {
        std::lock_guard lk { m_mutex };
        std::swap(m_queue, queue);
    }

    while (!queue.empty()) {
        auto& coro = queue.front();

        coro.schedule();

        queue.pop_front();
    }
}

inline waiting_queue::awaiter waiting_queue::
operator co_await() noexcept
{
    return awaiter { *this };
}

inline void waiting_queue::
enqueue(coroutine_ptr<> h)
{
    std::unique_lock lk { m_mutex };
    m_queue.push_back(h);
}

} // namespace stream9::coroutine::detail

#endif // STREAM9_COROUTINE_DETAIL_WAITING_LIST_HPP
