#ifndef STREAM9_COROUTINE_PROMISE_RESULT_HPP
#define STREAM9_COROUTINE_PROMISE_RESULT_HPP

#include "result.hpp"

#include <coroutine>
#include <exception>
#include <utility>

namespace stream9::coroutine {

template<typename T>
class promise_result
{
public:
    promise_result(class result<T>& s)
        : m_storage { s }
    {}

    void return_value(T& v)
    {
        m_storage.set_value(v);
    }

    void return_value(T const& v)
    {
        m_storage.set_value(v);
    }

    void return_value(T&& v)
    {
        m_storage.set_value(std::move(v));
    }

    std::suspend_always yield_value(T& v)
    {
        m_storage.set_value(v);
        return {};
    }

    std::suspend_always yield_value(T&& v)
    {
        m_storage.set_value(std::move(v));
        return {};
    }

    void unhandled_exception()
    {
        m_storage.set_error(std::current_exception());
    }

    T& result() &
    {
        return m_storage.get();
    }

    T&& result() &&
    {
        return std::move(m_storage.get());
    }

private:
    class result<T>& m_storage;
};

template<typename T>
class promise_result<T&>
{
public:
    promise_result(class result<T&>& s)
        : m_storage { s }
    {}

    void return_value(T& v)
    {
        m_storage.set_value(v);
    }

    std::suspend_always yield_value(T& v)
    {
        m_storage.set_value(v);
        return {};
    }

    void unhandled_exception()
    {
        m_storage.set_error(std::current_exception());
    }

    T& result()
    {
        return m_storage.get();
    }

private:
    class result<T&>& m_storage;
};

template<typename T>
class promise_result<T&&>
{
public:
    promise_result(class result<T&&>& s)
        : m_storage { s }
    {}

    void return_value(T v)
    {
        m_storage.set_value(std::move(v));
    }

    std::suspend_always yield_value(T v)
    {
        m_storage.set_value(std::move(v));
        return {};
    }

    void unhandled_exception()
    {
        m_storage.set_error(std::current_exception());
    }

    T&& result()
    {
        return m_storage.get();
    }

private:
    class result<T&&>& m_storage;
};

template<>
class promise_result<void>
{
public:
    promise_result(class result<void>& s)
        : m_storage { s }
    {}

    void return_void() noexcept {}

    void unhandled_exception() noexcept
    {
        m_storage.set_error(std::current_exception());
    }

    void result()
    {
        m_storage.get();
    }

private:
    class result<void>& m_storage;
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_PROMISE_RESULT_HPP
