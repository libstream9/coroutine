#ifndef STREAM9_COROUTINE_SCHEDULE_HPP
#define STREAM9_COROUTINE_SCHEDULE_HPP

#include "coroutine_ptr.hpp"
#include "task.hpp"

#include <stream9/errors.hpp>

#include <coroutine>

namespace stream9::coroutine {

class schedule_awaiter
{
public:
    schedule_awaiter(coroutine_ptr<> co)
        : m_coroutine { co }
    {}

    bool await_ready() const noexcept { return false; }

    template<typename P>
    bool
    await_suspend(std::coroutine_handle<P> h) const
    {
        try {
            auto& sched = h.promise().scheduler();
            sched.schedule(m_coroutine);
            return false;
        }
        catch (...) {
            rethrow_error();
        }
    }

    void await_resume() const {}

private:
    coroutine_ptr<> m_coroutine;
};

inline task<void>
schedule(coroutine_ptr<> co) noexcept
{
    return schedule_awaiter(co);
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_SCHEDULE_HPP
