#ifndef STREAM9_COROUTINE_TRANSFORM_HPP
#define STREAM9_COROUTINE_TRANSFORM_HPP

#include "type_traits.hpp"
#include "task.hpp"

#include <concepts>
#include <utility>

namespace stream9::coroutine {

namespace transform_ {

    template<typename F>
    class pipe
    {
    public:
        pipe(F& f) : m_fn { f } {}
        pipe(F&& f) : m_fn { std::move(f) } {}

        template<awaitable A>
            requires (std::invocable<F, awaitable_result_t<A>>)
        friend task<decltype(std::declval<F>()(std::declval<awaitable_result_t<A>>()))>
        operator|(A a, pipe const& fn)
        {
            co_return fn.m_fn(co_await a);
        }

    private:
        F m_fn;
    };

    struct api
    {
        template<awaitable A, std::invocable<awaitable_result_t<A>> F>
        task<decltype(std::declval<F>()(std::declval<awaitable_result_t<A>>()))>
        operator()(A&& a, F&& fn) const
        {
            co_return fn(co_await std::forward<A>(a));
        }

        template<typename F>
        pipe<F>
        operator()(F&& fn) const
        {
            return std::forward<F>(fn);
        }
    };

} // namespace transform_

inline constexpr transform_::api transform;

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_TRANSFORM_HPP
