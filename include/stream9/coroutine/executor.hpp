#ifndef STREAM9_COROUTINE_EXECUTOR_HPP
#define STREAM9_COROUTINE_EXECUTOR_HPP

#include <functional>

namespace stream9::coroutine {

class executor
{
public:
    virtual ~executor() = default;

    virtual void execute(std::function<void()>) = 0;
};

class inline_executor_t : public executor
{
public:
    void execute(std::function<void()> fn) override { fn(); }
};

inline auto&
inline_executor()
{
    static inline_executor_t instance {};
    return instance;
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_EXECUTOR_HPP
