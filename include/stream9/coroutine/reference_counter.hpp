#ifndef STREAM9_COROUTINE_REFERENCE_COUNTER_HPP
#define STREAM9_COROUTINE_REFERENCE_COUNTER_HPP

#include <atomic>
#include <cassert>

namespace stream9::coroutine {

class reference_counter
{
public:
    reference_counter() = default;

    reference_counter(std::ptrdiff_t const c) noexcept
        : m_count { c }
    {}

    std::ptrdiff_t value() const noexcept
    {
        return m_count;
    }

    std::ptrdiff_t ref() noexcept
    {
        return m_count.fetch_add(1, std::memory_order_relaxed);
    }

    std::ptrdiff_t unref() noexcept
    {
        assert(m_count.load() != 0);
        return m_count.fetch_sub(1, std::memory_order_acq_rel);
    }

private:
    std::atomic<std::ptrdiff_t> m_count = 0;
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_REFERENCE_COUNTER_HPP
