#ifndef STREAM9_COROUTINE_BASIC_SCHEDULER_HPP
#define STREAM9_COROUTINE_BASIC_SCHEDULER_HPP

#include "coroutine_ptr.hpp"
#include "executor.hpp"
#include "scheduler.hpp"
#include "task.hpp"
#include "type_traits.hpp"

#include <condition_variable>
#include <deque>
#include <mutex>
#include <vector>

namespace stream9::coroutine {

class basic_scheduler : public scheduler
{
public:
    basic_scheduler(class executor& = inline_executor());

    void schedule(coroutine_ptr<>) override;
    void schedule(lx::fd_ref src, uint32_t events, coroutine_ptr<>) override;

    void deschedule(coroutine_ptr<>) override;

    void add_suspended(coroutine_ptr<>);

    class executor& executor() const;
    void set_executor(class executor&);

    void run();

    void shutdown() noexcept;

private:
    coroutine_ptr<> next_ready();
    bool has_executing() const;
    bool has_waiting() const;
    void done_executing(coroutine_ptr<> const&);

private:
    class executor* m_executor = &inline_executor();
    std::vector<coroutine_ptr<>> m_suspended;
    std::deque<coroutine_ptr<>> m_ready;
    std::vector<coroutine_ptr<>> m_executing;
    std::mutex m_queue_mutex;
    std::condition_variable m_ready_event;
    bool m_stop_requested = false;
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_BASIC_SCHEDULER_HPP
