#ifndef STREAM9_COROUTINE_COROUTINE_PTR_HPP
#define STREAM9_COROUTINE_COROUTINE_PTR_HPP

#include "scheduler.hpp"
#include "promise_base.hpp"
#include "reference_counter.hpp"

#include <atomic>
#include <cassert>
#include <coroutine>
#include <memory>

namespace stream9::coroutine {

template<typename Promise = void>
class coroutine_ptr
{
public:
    coroutine_ptr() = default;

    coroutine_ptr(std::coroutine_handle<Promise> h)
        requires std::derived_from<Promise, promise_base>
        : m_handle { h }
        , m_promise_base { &h.promise() }
    {
        m_promise_base->ref();
    }

    template<typename T = Promise>
        requires (!std::same_as<T, void>)
              && std::derived_from<std::remove_cvref_t<T>, promise_base>
    coroutine_ptr(T& p)
        : m_handle { std::coroutine_handle<T>::from_promise(p) }
        , m_promise_base { &p }
    {
        m_promise_base->ref();
    }

    coroutine_ptr(std::coroutine_handle<void> h, promise_base& p) noexcept
        : m_handle { h }
        , m_promise_base { &p }
    {
        m_promise_base->ref();
    }

    ~coroutine_ptr() noexcept
    {
        if (m_promise_base && m_promise_base->unref() == 1) {
            if (m_handle) {
                m_handle.destroy();
            }
        }
    }

    coroutine_ptr(coroutine_ptr const& other) noexcept
        : m_handle { other.m_handle }
        , m_promise_base { other.m_promise_base }
    {
        m_promise_base->ref();
    }

    coroutine_ptr& operator=(coroutine_ptr const& other) noexcept
    {
        auto tmp { other };
        swap(tmp);
        return *this;
    }

    coroutine_ptr(coroutine_ptr&& other) noexcept
    {
        swap(other);
    }

    coroutine_ptr& operator=(coroutine_ptr&& other) noexcept
    {
        auto tmp { std::move(other) };
        swap(tmp);
        return *this;
    }

    auto handle() const noexcept { return m_handle; }

    bool done() const noexcept
    {
        return m_handle.done();
    }

    decltype(auto) promise() const
        noexcept(noexcept(std::declval<std::coroutine_handle<Promise>>().resume()))
        requires (!std::same_as<Promise, void>)
    {
        return m_handle.promise();
    }

    auto& scheduler() const noexcept { return m_promise_base->scheduler(); }

    void set_scheduler(class scheduler& s)
    {
        m_promise_base->set_scheduler(s);
    }

    void resume() const //TODO noexcept
    {
        assert(!done());

        m_handle.resume();
    }

    void schedule() const
    {
        assert(!done());

        scheduler().schedule(*this);
    }

    void swap(coroutine_ptr& other) noexcept
    {
        using std::swap;
        swap(m_handle, other.m_handle);
        swap(m_promise_base, other.m_promise_base);
    }

    void operator()() const noexcept(noexcept(resume())) { resume(); }

    explicit operator bool () const noexcept { return m_handle; }

    operator coroutine_ptr<> () const noexcept
    {
        return { m_handle, *m_promise_base };
    }

    bool operator==(coroutine_ptr const& other) const = default;

    bool operator==(std::nullptr_t) const
    {
        return m_handle == nullptr;
    }

private:
    std::coroutine_handle<Promise> m_handle = nullptr; // non-null
    promise_base* m_promise_base = nullptr; // non-null
};

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_COROUTINE_PTR_HPP
