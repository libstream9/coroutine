#ifndef STREAM9_COROUTINE_LATCH_HPP
#define STREAM9_COROUTINE_LATCH_HPP

#include "task.hpp"
#include "detail/waiting_queue.hpp"

#include <iostream> //TODO

namespace stream9::coroutine {

class latch
{
public:
    latch(std::ptrdiff_t count);

    latch(latch const&) = delete;
    latch& operator=(latch const&) = delete;

    // query
    bool try_wait();

    // command
    void count_down(std::ptrdiff_t n = 1);
    task<> wait();

    task<> arrive_and_wait(std::ptrdiff_t n = 1);

private:
    detail::waiting_queue m_queue;
    std::atomic_ptrdiff_t m_count {};
};

inline latch::
latch(std::ptrdiff_t const count)
    : m_count { count }
{}

inline bool latch::
try_wait()
{
    return m_count != 0;
}

inline void latch::
count_down(std::ptrdiff_t const n/*= 1*/)
{
    if (m_count == 0) return;

    m_count -= n;
    if (m_count <= 0) {

        m_queue.release_all();
    }
}

inline task<> latch::
wait()
{
    if (m_count != 0) {
        co_await m_queue;
    }
}

inline task<> latch::
arrive_and_wait(std::ptrdiff_t const n/*= 1*/)
{
    if (m_count != 0) {
        m_count -= n;
        if (m_count <= 0) {

            m_queue.release_all();
        }
        else {
            co_await m_queue;
        }
    }
}

} // namespace stream9::coroutine

#endif // STREAM9_COROUTINE_LATCH_HPP
